﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public delegate void onEndDelegate();
public class Timer
{
    public event onEndDelegate onEndTimerEvent;

    private int _countOfHours;
    private int _countOfMinutes;
    private int _countOfSeconds;
    private int _startHours;
    private int _startMinutes;
    private int _startSeconds;
    private DateTime _startTime;
    private bool _timerIsOn;
    private TimeSpan _tempTimeSpan;
    private string _formatOfTime = "00";

    public string TimerValueInString
    {
        get
        {
            if (_countOfHours != 0)
            {
                return _countOfHours.ToString(_formatOfTime) + ":" + _countOfMinutes.ToString(_formatOfTime) + ":" + _countOfSeconds.ToString(_formatOfTime);
            }
            else
            {
                return _countOfMinutes.ToString(_formatOfTime) + ":" + _countOfSeconds.ToString(_formatOfTime);
            }
			
        }
    }

    public bool IsOn
    {
        get
        {
            return _timerIsOn;
        }
    }

    public float TimerValueInDuration
    {
        get
        {
            return _countOfHours * 60 + _countOfMinutes * 60 + _countOfSeconds;
        }
    }

    public void InitializeNewTimer(int startMinutes, int startSeconds, int startHours = 0)
    {
        _tempTimeSpan = new TimeSpan(startHours, startMinutes, startSeconds);
        _startMinutes = startMinutes;
        _startSeconds = startSeconds;
        _startHours = startHours;
    }

    public void InitializeNewTimer(int duration)
    {
        InitializeNewTimer((int)duration / 60, duration % 60);
    }

    public void StartTimer()
    {
        _startTime = DateTime.Now;
        _timerIsOn = true;
    }

    public void PauseTimer()
    {
        _timerIsOn = false;
    }

    public void UpdateTimerValue()
    {
        if (_timerIsOn)
        {
            TimeSpan t = new TimeSpan();
            t = _tempTimeSpan - (DateTime.Now - _startTime);
            _countOfHours = t.Hours;
            _countOfMinutes = t.Minutes;
            _countOfSeconds = t.Seconds;
            if (_countOfSeconds == 0 && _countOfMinutes == 0)
            {
                _timerIsOn = false; 
                if (onEndTimerEvent != null)
                {
                    onEndTimerEvent();
                }
            }
        }
    }

    public void RemoveAllHandlers()
    {
        onEndTimerEvent = null;
    }

    public void EditCurrentValue(int countOfMinutes, int countOfSeconds, int countOfHours = 0)
    {
        PauseTimer();
        _countOfMinutes += countOfMinutes;
        if (_countOfMinutes < 0)
        {
            _countOfMinutes = 0;
        }
        _countOfSeconds += countOfSeconds;
        if (_countOfSeconds < 0)
        {
            _countOfSeconds = 0;
        }
        _countOfHours += countOfHours;
        if (_countOfHours < 0)
        {
            _countOfHours = 0;
        }
        InitializeNewTimer(_countOfMinutes, _countOfSeconds, _countOfHours);
        StartTimer();
    }

    public void EditCurrentValue(int duration)
    {
        EditCurrentValue((int)duration / 60, duration % 60);
    }
}
