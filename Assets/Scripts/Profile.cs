﻿using SQLite4Unity3d;

public class Profile
{

	[PrimaryKey, AutoIncrement]
    public int id { get; set; }
    public int count_coin { get; set; }
	public int current_ship { get; set; }
	public int rocket_classA { get; set; }
	public int rocket_classB { get; set; }
    public int max_distance { get; set; }
}
