﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CellInRewards : MonoBehaviour
{
    public delegate void OnSetRewards(int a);

    public static  OnSetRewards OnEditIconRewards;

    [SerializeField]
    private Button _buttonTake;

    [SerializeField]
    private eTypeOfRewards _typeRewards;
    [SerializeField]
    private Text _textCurrentState;

    void Start ()
    {
        _buttonTake.onClick.AddListener(TakeCoin);
    }
    private void OnEnable()
    {
        GetState();
    }

    void GetState()
    {
        if (_typeRewards == eTypeOfRewards.CountFights)
        {
            if (ProfilePlayer.GetCountFight == -1)
            {
                _textCurrentState.text = "---";
                _buttonTake.interactable = false;
            }
            else
            {
                if (ProfilePlayer.GetCountFight >= Constants.RewardsFight)
                {
                    _textCurrentState.text = ProfilePlayer.GetCountFight.ToString();
                    _buttonTake.interactable = true;
                }
                else
                {
                    _buttonTake.interactable = false;
                }
            }
        }
        if (_typeRewards == eTypeOfRewards.DistanceOneThousend)
        {
            if (ProfilePlayer.GetOneThousend == -1)
            {
                _textCurrentState.text = "---";
                _buttonTake.interactable = false;
            }
            else
            {
                if (ProfilePlayer.GetOneThousend >= Constants.RewardsOneThousend)
                {
                    _textCurrentState.text = ProfilePlayer.GetOneThousend.ToString();
                    _buttonTake.interactable = true;
                }
                else
                {
                    _buttonTake.interactable = false;
                }
            }

        }
        if (_typeRewards == eTypeOfRewards.DistanceTenThousend)
        {
            if (ProfilePlayer.GetTenThousend == -1)
            {
                _textCurrentState.text = "---";
                _buttonTake.interactable = false;
            }
            else
            {
                if (ProfilePlayer.GetTenThousend >= Constants.RewardsTenThousend)
                {
                    _textCurrentState.text = ProfilePlayer.GetTenThousend.ToString();
                    _buttonTake.interactable = true;
                }
                else
                {
                    _buttonTake.interactable = false;
                }
            }

        }
        if (_typeRewards == eTypeOfRewards.OpenShipFive)
        {
            if (ProfilePlayer.GetFiveShip == -1)
            {
                _textCurrentState.text = "---";
                _buttonTake.interactable = false;
            }
            else
            {
                InformAboutShip info = new InformAboutShip();

                info.GetInformationAboutShip(5);

                if (info.GetOpen == 1)
                {
                    _buttonTake.interactable = true;
                }
                else
                {
                    _buttonTake.interactable = false;
                }
            }

        }
        if (_typeRewards == eTypeOfRewards.OpenShipTen)
        {
            if (ProfilePlayer.GetTenShip == -1)
            {
                _textCurrentState.text = "---";
                _buttonTake.interactable = false;
            }
            else
            {
                InformAboutShip info = new InformAboutShip();

                info.GetInformationAboutShip(10);

                if (info.GetOpen == 1)
                {
                    _buttonTake.interactable = true;
                }
                else
                {
                    _buttonTake.interactable = false;
                }
            }
        }
    }
    void TakeCoin()
    {
        if (_typeRewards == eTypeOfRewards.CountFights)
        {
            ProfilePlayer.EditCountCoin(5000);
            ProfilePlayer.EditCountFights(-1);
        }
        if (_typeRewards == eTypeOfRewards.DistanceTenThousend)
        {
            ProfilePlayer.EditCountCoin(10000);
            ProfilePlayer.EditTenThousend(-1);
        }
        if (_typeRewards == eTypeOfRewards.OpenShipFive)
        {
            ProfilePlayer.EditCountCoin(7000);
            ProfilePlayer.EditFiveShip(-1);
        }
        if (_typeRewards == eTypeOfRewards.DistanceOneThousend)
        {
            ProfilePlayer.EditCountCoin(3000);
            ProfilePlayer.EditOneThousend(-1);
        }
        if (_typeRewards == eTypeOfRewards.OpenShipTen)
        {
            ProfilePlayer.EditCountCoin(15000);
            ProfilePlayer.EditTenShip(-1);
        }
        _buttonTake.gameObject.SetActive(false);
    }

}
