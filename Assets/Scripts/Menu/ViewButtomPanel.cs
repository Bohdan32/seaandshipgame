﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewButtomPanel : MonoBehaviour
{
    [SerializeField]
    private Button _btnStartGame;
    [SerializeField]
    private Button _btnOpenRatingWindow;
    [SerializeField]
    private ViewShipsSlider _viewSlider;


    private ViewFieldWindow _field;

    void Awake()
    {
        _btnOpenRatingWindow.onClick.AddListener(OpenRatingWindow);
        _btnStartGame.onClick.AddListener(StartGame);
    }

    private void OpenRatingWindow()
    {
        
    }

    private void StartGame()
    {
        UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.UIPanel);
        UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.UIPanel).GetComponent<ViewUIPanelWindow>().Construct();
        if (_field == null)
        {
            UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.Menu).GetComponent<ViewMenuWindow>().Close();
            _field = Instantiate(Resources.Load("Field") as GameObject).GetComponent<ViewFieldWindow>();
        }
        _field.Construct();

        _field.gameObject.SetActive(true);
    }
}
