﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewTopPanel : MonoBehaviour
{
    [SerializeField]
    private Button _btnExitGame;
    [SerializeField]
    private Text _textCountOfCoins;
    [SerializeField]
    private Button _btnOpenRewards;

    /*

         [SerializeField]
         private GameObject _rewardsOne;
         [SerializeField]
         private GameObject _rewardsTwo;
         [SerializeField]
         private GameObject _rewardsThree;
         [SerializeField]
         private GameObject _rewardsFour;
         [SerializeField]
         private GameObject _rewardsFive;
         [SerializeField]
         private GameObject _rewardsSix;
     */

    void Awake()
    {
        _btnExitGame.onClick.AddListener(ExitGame);
        _btnOpenRewards.onClick.AddListener(OpenRewardsWindow);
        CellInRewards.OnEditIconRewards = GetTrueRewards;
        ProfilePlayer.OnEditCountCoin += Construct;
    }

    private void OnEnable()
    {
        _textCountOfCoins.text = ProfilePlayer.GetCountCoin.ToString();
        /*
        if (ProfilePlayer.CurrentFights > 50)
        {
            _rewardsOne.SetActive(true);
        }
        if (ProfilePlayer.MaxDistance > 10000)
        {
            _rewardsTwo.SetActive(true);
        }
        if (ProfilePlayer.MaxDistance > 20000)
        {
            _rewardsThree.SetActive(true);
        }
        if (ProfilePlayer.CurrentShip == 5)
        {
            _rewardsFour.SetActive(true);
        }
        if (ProfilePlayer.CurrentShip == 10)
        {
            _rewardsFive.SetActive(true);
        }
        if (ProfilePlayer.WithoutBomdOneThousand > 1000)
        {
            _rewardsSix.SetActive(true);
        }
        */
    }
    void GetTrueRewards(int number)
    {
        /*
        switch (number)
        {
            case 1:
                _rewardsOne.SetActive(true);
                break;
            case 2:
                _rewardsTwo.SetActive(true);
                break;
            case 3:
                _rewardsThree.SetActive(true);
                break;
            case 4:
                _rewardsFour.SetActive(true);
                break;
            case 5:
                _rewardsFive.SetActive(true);
                break;
            case 6:
                _rewardsSix.SetActive(true);
                break;
        }
        */
    }
    public void Construct()
    {
        _textCountOfCoins.text = ProfilePlayer.GetCountCoin.ToString();
    }
    private void OpenRewardsWindow()
    {
        UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.WindowRewards);
    }

    private void ExitGame()
    {
        Application.Quit();
    }
}
