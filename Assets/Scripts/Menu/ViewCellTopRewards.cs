﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewCellTopRewards : MonoBehaviour
{
    [SerializeField]
    private eTypeOfRewards _typeRewards;
    [SerializeField]
    private GameObject _trueRewards;

    private void Awake()
    {
        ViewMainPanel.OnEditStateRewards += CheckState;
    }
    private void OnEnable()
    {
        CheckState();
    }

    public void CheckState()
    {
        if (_typeRewards == eTypeOfRewards.CountFights)
        {
            if (ProfilePlayer.GetCountFight == -1)
            {
                _trueRewards.SetActive(true);
            }
            else
            {
                if (ProfilePlayer.GetCountFight >= Constants.RewardsFight)
                {
                    _trueRewards.SetActive(true);
                }
                else
                {
                    _trueRewards.SetActive(false);
                }     
            }
        }
        if (_typeRewards == eTypeOfRewards.DistanceOneThousend)
        {
            if (ProfilePlayer.GetOneThousend == -1)
            {
                _trueRewards.SetActive(true);
            }
            else
            {
                if (ProfilePlayer.GetOneThousend >= Constants.RewardsOneThousend)
                {
                    _trueRewards.SetActive(true);
                }
                else
                {
                    _trueRewards.SetActive(false);
                }
            }
        }
        if (_typeRewards == eTypeOfRewards.DistanceTenThousend)
        {
            if (ProfilePlayer.GetTenThousend == -1)
            {
                _trueRewards.SetActive(true);
            }
            else
            {
                if (ProfilePlayer.GetTenThousend >= Constants.RewardsTenThousend)
                {
                    _trueRewards.SetActive(true);
                }
                else
                {
                    _trueRewards.SetActive(false);
                }
            }
        }
        if (_typeRewards == eTypeOfRewards.OpenShipFive)
        {
            InformAboutShip info = new InformAboutShip();

            info.GetInformationAboutShip(5);

            if (info.GetOpen == 1)
            {
                _trueRewards.SetActive(true);
            }
            else
            {
                _trueRewards.SetActive(false);
            }
        }
        if (_typeRewards == eTypeOfRewards.OpenShipTen)
        {
            InformAboutShip info = new InformAboutShip();

            info.GetInformationAboutShip(10);

            if (info.GetOpen == 1)
            {
                _trueRewards.SetActive(true);
            }
            else
            {
                _trueRewards.SetActive(false);
            }
        }

    }
}
