﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewShipsSlider : MonoBehaviour
{   
    [SerializeField]
    private Button _buttonNext;
    [SerializeField]
    private Button _buttonPrevious;
    [SerializeField]
    private GameObject _shipPanel;

    [SerializeField]
    private ViewMainPanel _viewMainPanel;

    int numberShip;


    void Awake()
    {
        _buttonNext.onClick.AddListener(NextShip);
        _buttonPrevious.onClick.AddListener(PreviousShip);
    }

    public void OnEnable()
    {
       
        numberShip = ProfilePlayer.GetCurrentShip;//1
        for (int i = 0; i < 10; i++)
        {
            
            if (i != numberShip-1)
            {
                _shipPanel.transform.GetChild(i).gameObject.SetActive(false);
            }
                
        }
        _shipPanel.transform.GetChild(numberShip-1).gameObject.SetActive(true);
    
        _viewMainPanel.ShowInformationAboutShip(numberShip);
        if(numberShip == 1)
        {
            _buttonPrevious.gameObject.SetActive(false);
        }
        if (numberShip == 9)
        {
            _buttonNext.gameObject.SetActive(false);
        }
    }

    void PreviousShip()
    {
        numberShip--;
        for (int i = 0; i < 10; i++)
        {
            if (i != numberShip - 1)
            {
                _shipPanel.transform.GetChild(i).gameObject.SetActive(false);
            }
        }
        if (numberShip-1 == 0)
        {
            _buttonPrevious.gameObject.SetActive(false);
        }
        else
        {
            _buttonPrevious.gameObject.SetActive(true);
        }
        _shipPanel.transform.GetChild(numberShip-1).gameObject.SetActive(true);

        _buttonNext.gameObject.SetActive(true);
        _viewMainPanel.ShowInformationAboutShip(numberShip);
    }

    void NextShip()
    {
        numberShip++;
        for (int i = 0; i < 10; i++)
        {
            if (i != numberShip - 1)
            {
                _shipPanel.transform.GetChild(i).gameObject.SetActive(false);
            }
        }
        if (numberShip - 1 == 9)
        {
            _buttonNext.gameObject.SetActive(false);
        }
        else
        {
            _buttonNext.gameObject.SetActive(true);
        }

        _shipPanel.transform.GetChild(numberShip-1).gameObject.SetActive(true);

        _buttonPrevious.gameObject.SetActive(true);

        _viewMainPanel.ShowInformationAboutShip(numberShip);
    }
    
}
