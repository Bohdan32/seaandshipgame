﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewLeftPanel : MonoBehaviour
{
    [SerializeField]
    private Button _btnRocketClass_1;
    [SerializeField]
    private Button _btnRocketClass_2;
    [SerializeField]
    private Button _btnCaliber;
    [SerializeField]
    private Text _textCountRocketClass_1;
    [SerializeField]
    private Text _textCountRocketClass_2;

    void Start ()
    {
        _btnCaliber.onClick.AddListener(BuyNewCaliber);
        _btnRocketClass_1.onClick.AddListener(BuyRocketClassA);
        _btnRocketClass_2.onClick.AddListener(BuyRocketClassB);
    }

    public void Construct(int countOfRocketA, int countOfRocketB)
    {
        _textCountRocketClass_1.text = countOfRocketA.ToString();
        _textCountRocketClass_2.text = countOfRocketB.ToString();
    }
    public void Enable(bool On)
    {
        _btnCaliber.interactable = On;
        _btnRocketClass_1.interactable = On;
        _btnRocketClass_2.interactable = On;
    }
    void BuyRocketClassA()
    {
        UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.WindowBuyRocketClassA);
    }

    void BuyRocketClassB()
    {
        UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.WindowBuyRocketClassB);
    }

    void BuyNewCaliber()
    {
        UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.WindowBuyNewCaliber);
    }
}
