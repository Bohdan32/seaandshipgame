﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewRewardsWindow : WindowViewBase
{
    /*
    [SerializeField]
    private Text _textCountFights;
    [SerializeField]
    private Text _textDistanceTenThousend;
    [SerializeField]
    private Text _textOpenShipFive;
    [SerializeField]
    private Text _textDistanceTwoThousend;
    [SerializeField]
    private Text _textDistanceWithoutBomb;
    [SerializeField]
    private Text _textOpenShipTen;
   
    [SerializeField]
    private Button _buttonCountFights;
    [SerializeField]
    private Button _buttonDistanceTenThousend;
    [SerializeField]
    private Button _buttonOpenShipFive;
    [SerializeField]
    private Button _buttonDistanceTwoThousend;
    [SerializeField]
    private Button _buttonDistanceWithoutBomb;
    [SerializeField]
    private Button _buttonOpenShipTen;
    */
    public override void OnEnable()
    {
        base.OnEnable();
       // _textCountFights.text = "Progress: " + ProfilePlayer.CurrentFights.ToString() + "/ 50";
       // _textDistanceTenThousend.text = "Progress: " + ProfilePlayer.MaxDistance.ToString() + "/ 10000";
       // _textOpenShipFive.text = "Progress: " + ProfilePlayer.CurrentShip.ToString() + "/ 5";
       // _textDistanceTwoThousend.text = "Progress :" + ProfilePlayer.MaxDistance.ToString() + "/ 20000";
       // _textDistanceWithoutBomb.text = "Progress :" + ProfilePlayer.MaxDistance.ToString() + "/ 1000";
       // _textOpenShipTen.text = "Progress :" + ProfilePlayer.CurrentShip.ToString() + "/ 10";
        CheckData();
    }

    void CheckData()
    {
        /*
        if (ProfilePlayer.CurrentFights > 50)
        {
            _buttonCountFights.interactable = true;
        }
        else
        {
            _buttonCountFights.interactable = false;
        }
        if (ProfilePlayer.MaxDistance > 10000)
        {
            _buttonDistanceTenThousend.interactable = true;
        }
        else
        {
            _buttonDistanceTenThousend.interactable = false;
        }
        if (ProfilePlayer.MaxDistance > 20000)
        {
            _buttonDistanceTwoThousend.interactable = true;
        }
        else
        {
            _buttonDistanceTwoThousend.interactable = false;
        }
        if (ProfilePlayer.CurrentShip == 5)
        {
            _buttonOpenShipFive.interactable = true;
        }
        else
        {
            _buttonOpenShipFive.interactable = false;
        }
        if (ProfilePlayer.CurrentShip == 10)
        {
            _buttonOpenShipTen.interactable = true;
        }
        else
        {
            _buttonOpenShipTen.interactable = false;
        }
        if (ProfilePlayer.WithoutBomdOneThousand > 1000)
        {
            _buttonDistanceWithoutBomb.interactable = true;
        }
        else
        {
            _buttonDistanceWithoutBomb.interactable = false;
        }
        */
    }
	void Start ()
    {
        buttonClose.onClick.AddListener(Close);
    }

    public override void Close()
    {
        UnityPoolManager.Instance.Push(LoadWindowManager.eWindowNameInLocation.WindowRewards, this);
    }
}
