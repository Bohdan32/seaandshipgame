﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StateImproveShip : MonoBehaviour {

    [SerializeField]
    private Text _textCurrentRocketClassA;
    [SerializeField]
    private Text _textCurrentRocketClassB;
    [SerializeField]
    private Text _textCurrentCaliber;
    [SerializeField]
    private Text _textCurrentArmor;
    [SerializeField]
    private Text _textCurrentAntiRocketSystem;
    [SerializeField]
    private Text _textCurrentEngine;

    public static StateImproveShip Instance;

    private void Awake()
    {
        Instance = this;
    }
    private void OnEnable()
    {
        _textCurrentRocketClassA.text = ProfilePlayer.GetRocketClassA.ToString();
        _textCurrentRocketClassB.text = ProfilePlayer.GetRocketClassB.ToString();
        _textCurrentCaliber.text = GetNumberState(ProfilePlayer.GetCaliber);
        _textCurrentArmor.text = GetNumberState(ProfilePlayer.GetArmor);
        _textCurrentAntiRocketSystem.text = GetNumberState(ProfilePlayer.GetAntiRocketSystem);
        _textCurrentEngine.text = GetNumberState(ProfilePlayer.GetEngineImproved);
    }


    string GetNumberState(int number)
    {
        string result;

        if (number == 1)
        {
            result = "I";
            return result;
        }
        if (number == 2)
        {
            result = "II";
            return result;
        }
        if (number == 3)
        {
            result = "III";
            return result;
        }
        if (number == 4)
        {
            result = "MAX";
            return result;
        }
        return "";

    }

    public void RefreshState()
    {
        _textCurrentRocketClassA.text = ProfilePlayer.GetRocketClassA.ToString();
        _textCurrentRocketClassB.text = ProfilePlayer.GetRocketClassB.ToString();
        _textCurrentCaliber.text = GetNumberState(ProfilePlayer.GetCaliber);
        _textCurrentArmor.text = GetNumberState(ProfilePlayer.GetArmor);
        _textCurrentAntiRocketSystem.text = GetNumberState(ProfilePlayer.GetAntiRocketSystem);
        _textCurrentEngine.text = GetNumberState(ProfilePlayer.GetEngineImproved);
    }

}
