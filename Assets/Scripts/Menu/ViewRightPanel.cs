﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewRightPanel : MonoBehaviour
{
    [SerializeField]
    private Button _buttonArmor;
    [SerializeField]
    private Button _buttonAntiRosketSystem;
    [SerializeField]
    private Button _buttonEngine;

    void Start()
    {
        _buttonArmor.onClick.AddListener(BuyArmor);
        _buttonAntiRosketSystem.onClick.AddListener(BuyAntiRocketSystem);
        _buttonEngine.onClick.AddListener(BuyEngine);

    }
    public void Enable(bool On)
    {
        _buttonArmor.interactable = On;
        _buttonAntiRosketSystem.interactable = On;
        _buttonEngine.interactable = On;
    }

    void BuyArmor()
    {
        UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.WindowBuyArmor);
    }

    void BuyAntiRocketSystem()
    {
        UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.WindowBuyAntiRocketSystem);
    }

    void BuyEngine()
    {
        UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.WindowBuyEngine);
    }

   
}
