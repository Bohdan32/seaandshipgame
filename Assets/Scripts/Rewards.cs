﻿using SQLite4Unity3d;


public class Rewards
{
    [PrimaryKey, AutoIncrement]
    public int id { get; set; }
    public int count_fight { get; set; }
    public int one_thousend { get; set; }
    public int ten_thousend { get; set; }
    public int five_ship { get; set; }
    public int ten_ship { get; set; }
    

}
