﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public delegate void EditCount();

public static class ProfilePlayer
{
    public static event EditCount OnEditCountCoin;

    //Table Profile
    private static int _count_coin;

    private static int _current_ship;

    private static int _rocket_classA;

    private static int _rocket_classB;

    private static int _max_distance;

    //Table ShipModel

    public static int _anti_rocket_system;

    public static int _armor;

    public static int _health;

    public static int _caliber;

    public static int _engine_improved;

    public static int _speed;

    public static int _open;

    public static int _price_ship;

    //Table Rewards

    public static int _count_fights;

    public static int _one_thousend;

    public static int _ten_thousend;

    public static int _five_ship;

    public static int _ten_ship;

    static ProfilePlayer()
    {
        
        var ds = new DataService("Game_db_2.db");

        var profile = ds.GetProfile();

        foreach (Profile pro in profile)
        {
            _count_coin = pro.count_coin;
            _current_ship = pro.current_ship;
            _rocket_classA = pro.rocket_classA;
            _rocket_classB = pro.rocket_classB;
            _max_distance = pro.max_distance;
        }

        var ms = new DataService("Game_db_2.db");

        var rewards = ms.GetRewards();

        foreach (Rewards rewa in rewards)
        {
            _count_fights = rewa.count_fight;
            _one_thousend = rewa.one_thousend;
            _ten_thousend = rewa.ten_thousend;
            _five_ship = rewa.five_ship;
            _ten_ship = rewa.ten_ship;
        }

        GetShipModel();

    }

    public static void GetShipModel()
    {
        var shh = new DataService("Game_db_2.db");

        var shipp = shh.GetShip(GetCurrentShip);

        foreach (ShipModel sh in shipp)
        {
            _anti_rocket_system = sh.anti_rocket_system;

            _armor = sh.armor;

            _health = sh.health;

            _caliber = sh.caliber;

            _engine_improved = sh.engine_improved;

            _speed = sh.speed;

            _open = sh.open;

            _price_ship = sh.price_ship;
        }
    }

    // Table Profile

    public static int GetCurrentShip
    {
        get
        {
            return _current_ship;
        }
        set
        {
            _current_ship = value;
        }
    }
    public static int GetCountCoin
    {
        get
        {
            return _count_coin;
        }
        set
        {
            _count_coin = value;
        }
    }
    public static int GetRocketClassA
    {
        get
        {
            return _rocket_classA;
        }
        set
        {
            _rocket_classA = value;
        }
    }
    public static int GetRocketClassB
    {
        get
        {
            return _rocket_classB;
        }
        set
        {
            _rocket_classB = value;
        }
    }
    public static int GetMaxDistance
    {
        get
        {
            return _max_distance;
        }
        set
        {
            _max_distance = value;
        }
    }



    public static void EditCountCoin(int count)
    {
        GetCountCoin = GetCountCoin + count;

        var ds = new DataService("Game_db_2.db");

        ds.UpdateProfile(GetCountCoin, GetCurrentShip, GetRocketClassA, GetRocketClassB, GetMaxDistance);
        Debug.Log("GetCountCoin = "+ GetCountCoin);
        if (OnEditCountCoin != null)
        {
            OnEditCountCoin();
        }
    }

    public static void EditCurrentShip(int current)
    {
        GetCurrentShip = current;

        var ds = new DataService("Game_db_2.db");

        ds.UpdateProfile(GetCountCoin, GetCurrentShip, GetRocketClassA, GetRocketClassB, GetMaxDistance);
    }
    public static void EditRocketClassA(int count)
    {
        GetRocketClassA = GetRocketClassA + count;

        var ds = new DataService("Game_db_2.db");

        ds.UpdateProfile(GetCountCoin, GetCurrentShip, GetRocketClassA, GetRocketClassB, GetMaxDistance);
    }
    public static void EditRocketClassB(int count)
    {
        GetRocketClassB = GetRocketClassB + count;

        var ds = new DataService("Game_db_2.db");

        ds.UpdateProfile(GetCountCoin, GetCurrentShip, GetRocketClassA, GetRocketClassB, GetMaxDistance);
    }
    public static void EditMaxDistance(int count)
    {
        GetMaxDistance = GetMaxDistance + count;

        var ds = new DataService("Game_db_2.db");

        ds.UpdateProfile(GetCountCoin, GetCurrentShip, GetRocketClassA, GetRocketClassB, GetMaxDistance);
    }

    // Table ShipModel
    public static int GetAntiRocketSystem
    {
        get
        {
            return _anti_rocket_system;
        }
        set
        {
            _anti_rocket_system = value;
        }
    }
    public static int GetArmor
    {
        get
        {
            return _armor;
        }
        set
        {
            _armor = value;
        }
    }
    public static int GetHealth
    {
        get
        {
            return _health;
        }
        set
        {
            _health = value;
        }
    }
    public static int GetCaliber
    {
        get
        {
            return _caliber;
        }
        set
        {
            _caliber = value;
        }
    }
    public static int GetEngineImproved
    {
        get
        {
            return _engine_improved;
        }
        set
        {
            _engine_improved = value;
        }
    }
    public static int GetSpeed
    {
        get
        {
            return _speed;
        }
        set
        {
            _speed = value;
        }
    }
    public static int GetOpen
    {
        get
        {
            return _open;
        }
        set
        {
            _open = value;
        }
    }
    public static int GetPriceShip
    {
        get
        {
            return _price_ship;
        }
        set
        {
            _price_ship = value;
        }
    }



    public static void EditAntiRocketSystem(int count)
    {
        GetAntiRocketSystem = count;

        var ds = new DataService("Game_db_2.db");

        ds.UpdateShipModel(GetCurrentShip,GetAntiRocketSystem, GetArmor, GetHealth, GetCaliber,GetEngineImproved, GetSpeed, GetOpen, GetPriceShip);
    }
    public static void EditArmor(int count)
    {
        GetArmor = count;

        var ds = new DataService("Game_db_2.db");

        ds.UpdateShipModel(GetCurrentShip,GetAntiRocketSystem, GetArmor, GetHealth, GetCaliber, GetEngineImproved, GetSpeed, GetOpen, GetPriceShip);
    }
    public static void EditHealth(int count)
    {
        GetHealth = GetHealth + count;

        var ds = new DataService("Game_db_2.db");

        ds.UpdateShipModel(GetCurrentShip,GetAntiRocketSystem, GetArmor, GetHealth, GetCaliber, GetEngineImproved, GetSpeed, GetOpen, GetPriceShip);
    }
    public static void EditCaliber(int count)
    {
        GetCaliber = count;

        var ds = new DataService("Game_db_2.db");

        ds.UpdateShipModel(GetCurrentShip,GetAntiRocketSystem, GetArmor, GetHealth, GetCaliber, GetEngineImproved, GetSpeed, GetOpen, GetPriceShip);
    }
    public static void EditEngineImproved(int count)
    {
        GetEngineImproved = count;

        var ds = new DataService("Game_db_2.db");

        ds.UpdateShipModel(GetCurrentShip,GetAntiRocketSystem, GetArmor, GetHealth, GetCaliber, GetEngineImproved, GetSpeed, GetOpen, GetPriceShip);
    }
    public static void EditSpeed(int count)
    {
        GetSpeed = GetSpeed + count;

        var ds = new DataService("Game_db_2.db");

        ds.UpdateShipModel(GetCurrentShip,GetAntiRocketSystem, GetArmor, GetHealth, GetCaliber, GetEngineImproved, GetSpeed, GetOpen, GetPriceShip);
    }
    public static void EditOpen(int b)
    {
        GetOpen = b;

        var ds = new DataService("Game_db_2.db");

        ds.UpdateShipModel(GetCurrentShip,GetAntiRocketSystem, GetArmor, GetHealth, GetCaliber, GetEngineImproved, GetSpeed, GetOpen, GetPriceShip);
    }
    public static void EditPriceShip(int count)
    {
        GetPriceShip = GetPriceShip + count;

        var ds = new DataService("Game_db_2.db");

        ds.UpdateShipModel(GetCurrentShip,GetAntiRocketSystem, GetArmor, GetHealth, GetCaliber, GetEngineImproved, GetSpeed, GetOpen, GetPriceShip);
    }





    //Table Rewards
    public static int GetCountFight
    {
        get
        {
            return _count_fights;
        }
        set
        {
            _count_fights = value;
        }
    }
    public static int GetOneThousend
    {
        get
        {
            return _one_thousend;
        }
        set
        {
            _one_thousend = value;
        }
    }
    public static int GetTenThousend
    {
        get
        {
            return _ten_thousend;
        }
        set
        {
            _ten_thousend = value;
        }
    }
    public static int GetFiveShip
    {
        get
        {
            return _five_ship;
        }
        set
        {
            _five_ship = value;
        }
    }
    public static int GetTenShip
    {
        get
        {
            return _ten_ship;
        }
        set
        {
            _ten_ship = value;
        }
    }


    public static void EditCountFights(int count)
    {
        GetCountFight = count;

        var ds = new DataService("Game_db_2.db");

        ds.UpdateRewards(GetCountFight,GetOneThousend,GetTenThousend,GetFiveShip,GetTenShip);
    }

    public static void EditOneThousend(int count)
    {
        GetOneThousend = count;

        var ds = new DataService("Game_db_2.db");

        ds.UpdateRewards(GetCountFight, GetOneThousend, GetTenThousend, GetFiveShip, GetTenShip);
    }
    public static void EditTenThousend(int count)
    {
        GetTenThousend = count;

        var ds = new DataService("Game_db_2.db");

        ds.UpdateRewards(GetCountFight, GetOneThousend, GetTenThousend, GetFiveShip, GetTenShip);
    }
    public static void EditFiveShip(int count)
    {
        GetFiveShip = count;

        var ds = new DataService("Game_db_2.db");

        ds.UpdateRewards(GetCountFight, GetOneThousend, GetTenThousend, GetFiveShip, GetTenShip);
    }
    public static void EditTenShip(int count)
    {
        GetTenShip = count;

        var ds = new DataService("Game_db_2.db");

        ds.UpdateRewards(GetCountFight, GetOneThousend, GetTenThousend, GetFiveShip, GetTenShip);
    }
   
}