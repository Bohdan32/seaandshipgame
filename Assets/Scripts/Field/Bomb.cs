﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bomb : MonoBehaviour
{
    [SerializeField]
    private GameObject _gameObjectOfExplosion;

    public eTypeOfBomb _typeOfBomb;

    private Vector3 _startPosition;

    private Plane _plane;

    private Rigidbody2D _rigidbody;

    void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _startPosition = gameObject.transform.localPosition;
        _plane = transform.parent.GetComponent<Plane>();
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        
        Ship ship = coll.gameObject.GetComponent<Ship>();
        if (ship != null)
        {
            if (ship.GetDamage((int)_typeOfBomb+1))
            {
                StartCoroutine(Explosion());
            }
        }
        else
        {
            if (coll.gameObject.tag == "BombProtection")
            {
                StartCoroutine(Explosion());
            }
        }

    }

    public void Shoot()
    {
        gameObject.SetActive(true);
        transform.parent = _plane.transform.parent;
        _rigidbody.velocity = Vector2.zero;
    }

    IEnumerator Explosion()
    {
        
        _gameObjectOfExplosion.SetActive(true);
        yield  return new WaitForSeconds(0.07f);
        _gameObjectOfExplosion.SetActive(false);
        gameObject.SetActive(false);
        transform.parent = _plane.transform;
        gameObject.transform.localPosition = _startPosition;

    }
}
