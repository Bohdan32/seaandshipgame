﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewButtonUseRocket : MonoBehaviour
{
    public eTypeOfBullet _typeOfRocket;
    [SerializeField]
    private Button _btnUseRocket;

    void Start()
    {
        _btnUseRocket.onClick.AddListener(UseRocket);
    }

    private void UseRocket()
    {
        Ship.Instance.SetRocket(_typeOfRocket);
    }
}
