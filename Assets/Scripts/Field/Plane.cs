﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plane : MonoBehaviour
{

    [SerializeField]
    private Bomb _bomb;

    private float _health=500;

    private Vector3 _startPosition;

    private SpriteRenderer _sp;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "ControllerBomb")
        {
            _bomb.Shoot();
        }
    }

    public void GetDamage(float damage)
    {
        _health -= damage;

        if (_health <= 0)
        {
            if(_sp==null)
            {
                _sp= GetComponent<SpriteRenderer>();
            }
            if (_sp.flipX)
            {

                ControllerPlane.Instance.GetNewPlaneRight();
                gameObject.transform.position = ControllerPlane.Instance.GetNewPositionRight();
            }
            else
            {
                ControllerPlane.Instance.GetNewPlaneLeft();
                gameObject.transform.position = ControllerPlane.Instance.GetNewPositionLeft();
            }
        }
    }


}
