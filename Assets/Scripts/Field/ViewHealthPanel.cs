﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewHealthPanel : MonoBehaviour
{
    [SerializeField]
    public Image _healthBar;


    private float _maxHealth;

    private void Start()
    {
        
        Ship.Instance.OnEditCurrentHealth += EditHealth;
    }

    public void Construct(float healthOfShip)
    {
        _maxHealth = healthOfShip;
        _healthBar.fillAmount = 1;
    }

    public void EditHealth(float currentHealth)
    {
        _healthBar.fillAmount = currentHealth / _maxHealth;
    }

}
