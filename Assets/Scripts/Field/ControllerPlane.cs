﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ControllerPlane : MonoBehaviour
{
    public GameObject[] _arrayPlaneLeft = new GameObject[3];
    public GameObject[] _arrayPlaneRight = new GameObject[3];

    private GameObject[] _arrayholeLeft = new GameObject[3];
    private GameObject[] _arrayholeRight = new GameObject[3];

    [SerializeField]
    private RectTransform _panelHolesLeft;
    [SerializeField]
    private RectTransform _panelHolesRight;
    [SerializeField]
    private GameObject _holeLeft;
    [SerializeField]
    private GameObject _holeRight;


    private float speedPlanLeft;
    private float speedPlanRight;

    private int whoMoveLeft;
    private int whoMoveRight;

    public static ControllerPlane Instance;

    private float widhtBone;

    private void Awake()
    {
        Instance = this;
        FirstPosition();
    }

    public void SetNewSpeedtBone(float speed)
    {
        speedPlanLeft = speed;
    }

    private void OnEnable()
    {
        _arrayPlaneLeft[0].transform.position = GetNewPositionLeft();
        _arrayPlaneLeft[1].transform.position = GetNewPositionLeft();
        _arrayPlaneLeft[2].transform.position = GetNewPositionLeft();
        whoMoveLeft = 1;
        _arrayPlaneLeft[whoMoveLeft].SetActive(true);


        _arrayPlaneRight[0].transform.position = GetNewPositionRight();
        _arrayPlaneRight[1].transform.position = GetNewPositionRight();
        _arrayPlaneRight[2].transform.position = GetNewPositionRight();
        whoMoveRight = 0; 
        _arrayPlaneRight[whoMoveRight].SetActive(true);


    }

    public GameObject GetNewPlaneLeft()
    {
        _arrayPlaneLeft[whoMoveLeft].SetActive(false);
        whoMoveLeft = Random.Range(0, _arrayholeLeft.Length);
        _arrayPlaneLeft[whoMoveLeft].SetActive(true);
        return _arrayPlaneLeft[whoMoveLeft];
    }

    public GameObject GetNewPlaneRight()
    {
        _arrayPlaneRight[whoMoveRight].SetActive(false);
        whoMoveRight = Random.Range(0, _arrayholeRight.Length);
        _arrayPlaneRight[whoMoveRight].SetActive(true);
        return _arrayPlaneRight[whoMoveRight];
    }

    public Vector3 GetNewPositionLeft()
    {
        int left = Random.Range(0, _arrayholeLeft.Length);
        switch (left)
        {
            case 0:
                speedPlanLeft =  0.07f;
                break;
            case 1:
                speedPlanLeft =0.05f;
                break;
            case 2:
                speedPlanLeft =  0.01f;
                break;
            default:
                break;
        }
        return _arrayholeLeft[left].transform.position + Vector3.left * 2; 

    }

    public Vector3 GetNewPositionRight()
    {
        int right = Random.Range(0, _arrayholeRight.Length);
        switch (right)
        {
            case 0:
                speedPlanRight = 0.07f;
                break;
            case 1:
                speedPlanRight = 0.05f;
                break;
            case 2:
                speedPlanRight = 0.01f;
                break;
            default:
                break;
        }
        return _arrayholeRight[right].transform.position + Vector3.right * 2;

    }

    public void FirstPosition()
    {
        float height = _panelHolesLeft.rect.height;

        for (int i = 0; i < 3; i++)
        {
            GameObject go = Instantiate(_holeLeft);
            go.transform.parent = _holeLeft.transform.parent;
            go.transform.localScale = new Vector3(1, 1);
            go.SetActive(false);
            go.transform.localPosition = new Vector3(0, _panelHolesLeft.rect.height / 8 + _panelHolesLeft.rect.height * i / 4 - _panelHolesLeft.rect.height / 4, 0);
            _arrayholeLeft[i] = go;
        }
        for (int j = 0; j < 3; j++)
        {
            GameObject go1 = Instantiate(_holeRight);
            go1.transform.parent = _holeRight.transform.parent;
            go1.transform.localScale = new Vector3(1, 1);
            go1.SetActive(false);
            go1.transform.localPosition = new Vector3(0, _panelHolesLeft.rect.height / 8 + _panelHolesLeft.rect.height * j / 4 - _panelHolesLeft.rect.height / 4, 0);
            _arrayholeRight[j] = go1;
        }
    }

    private void Update()
    {
        _arrayPlaneLeft[whoMoveLeft].transform.Translate(Vector2.right * speedPlanLeft);
        if (_arrayPlaneLeft[whoMoveLeft].transform.position.x > _arrayholeRight[0].transform.position.x + 2)
        {
            GetNewPlaneLeft();
            _arrayPlaneLeft[whoMoveLeft].transform.position = GetNewPositionLeft();
        }

        _arrayPlaneRight[whoMoveRight].transform.Translate(Vector2.left * speedPlanRight);
        if (_arrayPlaneRight[whoMoveRight].transform.position.x < _arrayholeLeft[0].transform.position.x - 2)
        {
            GetNewPlaneRight();
            _arrayPlaneRight[whoMoveRight].transform.position = GetNewPositionRight();
        }

    }


}

