﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Rocket : MonoBehaviour
{


    bool IsFlying;
    Vector2 _destination;
    float _speed = 1f;
    private float _damage = 100f;
    public eTypeOfBullet _typeOfRocket;

    public void Shoot(Vector2 destination)
    {
        _destination = new Vector2( destination.x - gameObject.transform.localPosition.x, destination.y - gameObject.transform.localPosition.y);
        IsFlying = true;
        Destroy(gameObject, 3f);
    }

    void Update()
    {
        if (IsFlying)
        {
            gameObject.transform.Translate(_speed * _destination / _destination.magnitude);
        }
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
 
        Plane plane = coll.gameObject.GetComponent<Plane>();
        if (plane != null)
        {
            plane.GetDamage(_damage*((int)_typeOfRocket+1));
            Destroy(gameObject);
        }

    }
}
