﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ViewDistancePanel : MonoBehaviour
{
    [SerializeField]
    private Text _txtDistance;
    public static ViewDistancePanel Instance;
    private DateTime _startTime;
    private TimeSpan _currentTime;

    private bool _timerIsOn;
    private float _speed;
    public float CurrentDistance;

    void Awake()
    {
        Instance = this;
    }

    public void Construct(float speedOfShip)
    {
        _speed = speedOfShip;
        _startTime = DateTime.Now;
        _timerIsOn = true;
    }

    public void PauseTimer()
    {
        _timerIsOn = false;
    }

    void Update()
    {
        if (_timerIsOn)
        {
            _currentTime = DateTime.Now - _startTime;
            CurrentDistance = (_currentTime.Minutes * 60 + _currentTime.Seconds) * _speed;
            _txtDistance.text = CurrentDistance.ToString();
        }
    }
}
