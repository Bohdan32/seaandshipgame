﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewProtectionPanel : MonoBehaviour
{

    [SerializeField]
    private Button _btnUseProtection;
    private Timer _timer = new Timer();
    private float _duration;

    private void Awake()
    {
        _btnUseProtection.onClick.AddListener(UseProtection);
        _timer.onEndTimerEvent += EndTimer;
    }

    private void UseProtection()
    {
        Ship.Instance.ProtectShip(_duration);
        _btnUseProtection.interactable = false;
        _timer.InitializeNewTimer(50);
        _timer.StartTimer();
    }

    void Update()
    {
        _timer.UpdateTimerValue();
    }

    private void EndTimer()
    {
        _btnUseProtection.interactable = true;
    }

    public void Construct(float durationOfProtection)
    {
        _duration = durationOfProtection;
        _btnUseProtection.interactable = true;
    }
}
