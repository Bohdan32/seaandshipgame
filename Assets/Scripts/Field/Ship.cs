﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public delegate void OnEdit(float current);
public class Ship : MonoBehaviour
{
    public event OnEdit OnEditCurrentHealth;

    private Vector3 localPosition;

    [SerializeField]
    public Rocket _simpleBulletBase;
    [SerializeField]
    public Rocket _smallRocketBase;
    [SerializeField]
    public Rocket _bigRocketBase;
    [SerializeField]
    public GameObject _tool;
    [SerializeField]
    public GameObject _shipProtection;

    public static Ship Instance;

    private float _health;

    private eTypeOfBullet _activeTypeOfBullet;

    private Timer _timerOfShooting = new Timer();

    private int _countOfShoots = 0;

    private bool _readyToShoot = true;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        localPosition = gameObject.transform.localPosition;
        _timerOfShooting.onEndTimerEvent += EndTimer;
    }
    void Update()
    {
        _timerOfShooting.UpdateTimerValue();
    }
    public void Construct(float health)
    {
        _health = health;
        _activeTypeOfBullet = eTypeOfBullet.Simple;
        _shipProtection.SetActive(false);
    }

    public void SetRocket(eTypeOfBullet bulletType)
    {
        _activeTypeOfBullet = bulletType;
    }

    public bool GetDamage(float damage)
    {
        _health -= damage;

        if (_health < 0)
        {
            _health = 0;
        }
        if (OnEditCurrentHealth != null)
        {
            OnEditCurrentHealth(_health);
        }

        if (_health == 0)
        {
            ViewFieldWindow.Instance.GameOver();
            return false;
        }
        return true;
    }

    public void Shoot(Vector3 localClick)
    {
        if (_readyToShoot)
        {
            GameObject rocket;
            switch (_activeTypeOfBullet)
            {
                case eTypeOfBullet.SmallBomb:
                    rocket = Instantiate(_smallRocketBase.gameObject) as GameObject;
                    break;
                case eTypeOfBullet.BigBomb:
                    rocket = Instantiate(_bigRocketBase.gameObject) as GameObject;
                    break;
                default:
                    rocket = Instantiate(_simpleBulletBase.gameObject) as GameObject;
                    break;
            }

            rocket.transform.parent = gameObject.transform;
            rocket.transform.localScale = new Vector2(1f, 1f);
            Rocket rocketScript = rocket.GetComponent<Rocket>();
            rocket.transform.position = _tool.transform.position;
            rocket.SetActive(true);
            rocketScript.Shoot(localClick);
            _countOfShoots++;
            if (_countOfShoots > 10)
            {
                _timerOfShooting.InitializeNewTimer(3);
                _timerOfShooting.StartTimer();
                _readyToShoot = false;
            }
        }
    }
    void EndTimer()
    {
        _readyToShoot = true;
        _countOfShoots = 0;
    }
    public void ProtectShip(float duration)
    {
        StartCoroutine(Protect(duration));
    }

    IEnumerator Protect(float duration)
    {
        _shipProtection.SetActive(true);
        yield  return new WaitForSeconds(duration);
        _shipProtection.SetActive(false);
    }
}

public enum eTypeOfBullet
{
    Simple,
    SmallBomb,
    BigBomb
}

