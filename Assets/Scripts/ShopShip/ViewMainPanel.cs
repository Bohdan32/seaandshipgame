﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public delegate void EditStateRewards();

public class ViewMainPanel : MonoBehaviour
{
    public static event EditStateRewards OnEditStateRewards;

    public static ViewMainPanel Instance; 

    [SerializeField]
    private Button _btnBuyShip;

    [SerializeField]
    private Button _btnSelectShip;

    [SerializeField]
    private Text _textNameShip;

    [SerializeField]
    private Text _textPrice;

    [SerializeField]
    private ViewLeftPanel _leftPanel;

    [SerializeField]
    private ViewRightPanel _rightPanel;

    [SerializeField]
    private StateImproveShip _stateImprovePanel;

    private int numberShipShow;

    void Awake()
    {
        Instance = this;
        _btnBuyShip.onClick.AddListener(BuyNewShip);
        _btnSelectShip.onClick.AddListener(SelectShip);
    }
    
    public void ShowInformationAboutShip(int numberShip)
    {
        numberShipShow = numberShip;

        InformAboutShip info = new InformAboutShip();

        info.GetInformationAboutShip(numberShip);

        if (info.GetOpen == 1)
        {
            if (ProfilePlayer.GetCurrentShip == numberShip)
            {
               
                _leftPanel.Enable(true);
                _rightPanel.Enable(true);
                _btnBuyShip.gameObject.SetActive(false);
                _btnSelectShip.gameObject.SetActive(false);
            }
            else
            {
                
                _leftPanel.Enable(false);
                _rightPanel.Enable(false);
                _btnBuyShip.gameObject.SetActive(false);
                _btnSelectShip.gameObject.SetActive(true);
            }
        }
        else
        {
            
            _leftPanel.Enable(false);
            _rightPanel.Enable(false);
            _btnBuyShip.gameObject.SetActive(true);
            _textPrice.text = info.GetPriceShip.ToString();
            _btnSelectShip.gameObject.SetActive(false);
            if (ProfilePlayer.GetCountCoin >= info.GetPriceShip)
            {
                _btnBuyShip.interactable = true;
            }
            else
            {
                _btnBuyShip.interactable = false;
            }
        }

    }

    void BuyNewShip()
    {
        InformAboutShip info = new InformAboutShip();

        info.GetInformationAboutShip(numberShipShow);

        ProfilePlayer.EditCountCoin(-info.GetPriceShip);

        ProfilePlayer.EditCurrentShip(numberShipShow);

        ProfilePlayer.GetShipModel();

        ProfilePlayer.EditOpen(1);

        _leftPanel.Enable(true);

        _rightPanel.Enable(true);

        _btnBuyShip.gameObject.SetActive(false);

        _btnSelectShip.gameObject.SetActive(false);

        _stateImprovePanel.RefreshState();
        if (numberShipShow == 10)
        {
            if (OnEditStateRewards != null)
            {
                OnEditStateRewards();
            }
        }
        if (numberShipShow == 5)
        {
            if (OnEditStateRewards != null)
            {
                OnEditStateRewards();
            }
        }
    }

    void SelectShip()
    {
        ProfilePlayer.EditCurrentShip(numberShipShow);

        ProfilePlayer.GetShipModel();

        _btnSelectShip.gameObject.SetActive(false);

        _btnBuyShip.gameObject.SetActive(false);

        _leftPanel.Enable(true);

        _rightPanel.Enable(true);

        _stateImprovePanel.RefreshState();
    }

  
}
