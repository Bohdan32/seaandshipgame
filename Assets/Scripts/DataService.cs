﻿using SQLite4Unity3d;
using UnityEngine;
#if !UNITY_EDITOR
using System.Collections;
using System.IO;
#endif
using System.Collections.Generic;

public class DataService  {

	private SQLiteConnection _connection;

	public DataService(string DatabaseName){

#if UNITY_EDITOR
            var dbPath = string.Format(@"Assets/StreamingAssets/{0}", DatabaseName);
#else
        // check if file exists in Application.persistentDataPath
        var filepath = string.Format("{0}/{1}", Application.persistentDataPath, DatabaseName);

        if (!File.Exists(filepath))
        {
            Debug.Log("Database not in Persistent path");
            // if it doesn't ->
            // open StreamingAssets directory and load the db ->

#if UNITY_ANDROID 
            var loadDb = new WWW("jar:file://" + Application.dataPath + "!/assets/" + DatabaseName);  // this is the path to your StreamingAssets in android
            while (!loadDb.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
            // then save to Application.persistentDataPath
            File.WriteAllBytes(filepath, loadDb.bytes);
#elif UNITY_IOS
                 var loadDb = Application.dataPath + "/Raw/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
                // then save to Application.persistentDataPath
                File.Copy(loadDb, filepath);
#elif UNITY_WP8
                var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
                // then save to Application.persistentDataPath
                File.Copy(loadDb, filepath);

#elif UNITY_WINRT
		var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);
#else
	var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
	// then save to Application.persistentDataPath
	File.Copy(loadDb, filepath);

#endif

            Debug.Log("Database written");
        }

        var dbPath = filepath;
#endif
            _connection = new SQLiteConnection(dbPath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create);
	}
	public IEnumerable<Profile> GetProfile()
    {
		return _connection.Table<Profile>();
	}
    public IEnumerable<Rewards> GetRewards()
    {
        return _connection.Table<Rewards>();
    }
    public IEnumerable<ShipModel> GetShip(int currentShip)
    {
        return _connection.Table<ShipModel>().Where(x => x.id == currentShip);
    }

    public Profile UpdateProfile( int value1, int value2, int value3, int value4, int value5)
    {
        var p = new Profile
        {
            count_coin = value1,
            current_ship = value2,
            rocket_classA = value3,
            rocket_classB = value4,
            max_distance = value5
        };

        _connection.InsertOrReplace(p);
        return p;
    }

    public ShipModel UpdateShipModel(int value0, int value1, int value2, int value3, int value4, int value5, int value6, int value7, int value8)
    {
        var s = new ShipModel
        {
            id = value0,
            anti_rocket_system = value1,
            armor = value2,
            health = value3,
            caliber = value4,
            engine_improved = value5,
            speed = value6,
            open = value7,
            price_ship = value8
        };
        _connection.InsertOrReplace(s);
        return s;
    }
    public Rewards UpdateRewards(int value1, int value2, int value3, int value4, int value5)
    {
        var g = new Rewards
        {
            count_fight = value1,
            one_thousend = value2,
            ten_thousend = value3,
            five_ship = value4,
            ten_ship = value5,

        };
        _connection.InsertOrReplace(g);
        return g;
    }

}
