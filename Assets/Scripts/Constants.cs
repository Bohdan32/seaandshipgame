﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants 
{
    public const int CountCoinArmorTypeTwo = 2000;
    public const int CountCoinArmorTypeThree = 3000;
    public const int CountCoinArmorTypeFour = 4000;

    public const int CountCoinCaliberTypeTwo = 2000;
    public const int CountCoinCaliberTypeThree = 3000;
    public const int CountCoinCaliberTypeFour = 4000;

    public const int CountCoinEngineTypeTwo = 2000;
    public const int CountCoinEngineTypeThree = 3000;
    public const int CountCoinEngineTypeFour = 4000;

    public const int CountCoinAntiRocketSystemTypeTwo = 2000;
    public const int CountCoinAntiRocketSystemTypeThree = 3000;
    public const int CountCoinAntiRocketSystemTypeFour = 4000;

    public const int CountCoinRocketTypeOne = 100;
    public const int CountCoinRocketTypeTwo = 200;

    public const int CountCoinShip_2 = 10000;
    public const int CountCoinShip_3 = 20000;
    public const int CountCoinShip_4 = 30000;
    public const int CountCoinShip_5 = 40000;
    public const int CountCoinShip_6 = 50000;
    public const int CountCoinShip_7 = 60000;
    public const int CountCoinShip_8 = 70000;
    public const int CountCoinShip_9 = 80000;
    public const int CountCoinShip_10 = 90000;

    public const int TimeOfLife = 20;
    public const float SpeedOfBone = 0.2f;
    public const int TimeOfJumpPuppy = 2;
    public const int TimeOfJumpDog = 2;
    public const int TimeOfJumpBigDog = 1;
    public const int TimeAddByBone = 10;
    public const int TimeTakesByBone = 10;
    public const int StartCountCoin = 10000;
    public const int StartCountLive = 5;


    // Date for Rewards
    public const int RewardsFight = 100;
    public const int RewardsOneThousend = 1000;
    public const int RewardsTenThousend = 10000;

}

public enum eTypeOfBomb
{
    EasyBomb,
    MiddleBomb,
    HardBomb
}

public enum eTypeOfRewards
{
    CountFights,
    DistanceTenThousend,
    OpenShipFive,
    DistanceOneThousend,
    OpenShipTen,
    ReatingInGame
}
