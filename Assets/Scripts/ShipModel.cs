﻿using SQLite4Unity3d;

public class ShipModel
{

    [PrimaryKey, AutoIncrement]
    public int id { get; set; }
    public int anti_rocket_system { get; set; }
    public int armor { get; set; }
    public int health { get; set; }
    public int caliber { get; set; }
    public int engine_improved { get; set; }
    public int speed { get; set; }
    public int open { get; set; }
    public int price_ship { get; set; }

}
