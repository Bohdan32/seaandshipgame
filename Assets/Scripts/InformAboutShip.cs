﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InformAboutShip : MonoBehaviour
{
    public  int _anti_rocket_system;

    public  int _armor;

    public  int _health;

    public  int _caliber;

    public  int _engine_improved;

    public  int _speed;

    public  int _open;

    public  int _price_ship;


    public  int numberShip;

    public  void GetInformationAboutShip(int numberShips)
    {
        numberShip = numberShips;
        var shh = new DataService("Game_db_2.db");

        var shipp = shh.GetShip(numberShips);

        foreach (ShipModel sh in shipp)
        {
            _anti_rocket_system = sh.anti_rocket_system;

            _armor = sh.armor;

            _health = sh.health;

            _caliber = sh.caliber;

            _engine_improved = sh.engine_improved;

            _speed = sh.speed;

            _open = sh.open;

            _price_ship = sh.price_ship;
        }
    }
    public  int GetNumberShip
    {
        get
        {
            return numberShip;
        }
        set
        {
            numberShip = value;
        }
    }

    public  int GetAntiRocketSystem
    {
        get
        {
            return _anti_rocket_system;
        }
        set
        {
            _anti_rocket_system = value;
        }
    }
    public  int GetArmor
    {
        get
        {
            return _armor;
        }
        set
        {
            _armor = value;
        }
    }
    public  int GetHealth
    {
        get
        {
            return _health;
        }
        set
        {
            _health = value;
        }
    }
    public  int GetCaliber
    {
        get
        {
            return _caliber;
        }
        set
        {
            _caliber = value;
        }
    }
    public  int GetEngineImproved
    {
        get
        {
            return _engine_improved;
        }
        set
        {
            _engine_improved = value;
        }
    }
    public  int GetSpeed
    {
        get
        {
            return _speed;
        }
        set
        {
            _speed = value;
        }
    }
    public  int GetOpen
    {
        get
        {
            return _open;
        }
        set
        {
            _open = value;
        }
    }
    public  int GetPriceShip
    {
        get
        {
            return _price_ship;
        }
        set
        {
            _price_ship = value;
        }
    }

   
}
