﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewWindowBuyAntiRocketSystem : WindowViewBase
{

    [SerializeField]
    private Button _buttonBuy;
    [SerializeField]
    private Text _textCurrentStateImprovement;
    [SerializeField]
    private Text _textPriceImprovement;
    [SerializeField]
    private Text _textDescriptionImprovement;
   

    void Start()
    {
        buttonClose.onClick.AddListener(Close);
       _buttonBuy.onClick.AddListener(BuyInmprovement);
    }
    private void OnEnable()
    {
        base.OnEnable();
       _textCurrentStateImprovement.text = ProfilePlayer.GetAntiRocketSystem.ToString() + "/4";
       
        switch (ProfilePlayer.GetAntiRocketSystem)
        {
            case 1:
                _textPriceImprovement.text = Constants.CountCoinAntiRocketSystemTypeTwo.ToString();
                _textDescriptionImprovement.text = "+15 second for use this system";
                if (ProfilePlayer.GetCountCoin >= Constants.CountCoinAntiRocketSystemTypeTwo)
                {
                    _buttonBuy.interactable = true;
                }
                else
                {
                    _buttonBuy.interactable = false;
                }
                break;
            case 2:
                _textPriceImprovement.text = Constants.CountCoinAntiRocketSystemTypeThree.ToString();
                _textDescriptionImprovement.text = "+10 second for use this system";
                if (ProfilePlayer.GetCountCoin >= Constants.CountCoinAntiRocketSystemTypeThree)
                {
                    _buttonBuy.interactable = true;
                }
                else
                {
                    _buttonBuy.interactable = false;
                }
                break;
            case 3:
                _textPriceImprovement.text = Constants.CountCoinAntiRocketSystemTypeFour.ToString();
                _textDescriptionImprovement.text = "+10 second for use this system";
                if (ProfilePlayer.GetCountCoin >= Constants.CountCoinAntiRocketSystemTypeFour)
                {
                    _buttonBuy.interactable = true;
                }
                else
                {
                    _buttonBuy.interactable = false;
                }
                break;
            case 4:
                _buttonBuy.interactable = false;
                _textDescriptionImprovement.text = "Max time";
                _textPriceImprovement.text = 0.ToString();
                break;
        }
        
    }
    public override void Close()
    {
        UnityPoolManager.Instance.Push(LoadWindowManager.eWindowNameInLocation.WindowBuyAntiRocketSystem, this);
    }

    void BuyInmprovement()
    {
        
        switch (ProfilePlayer.GetAntiRocketSystem)
        {

            case 1:
                ProfilePlayer.EditCountCoin(-Constants.CountCoinAntiRocketSystemTypeTwo);
                ProfilePlayer.EditAntiRocketSystem(2);
                _textPriceImprovement.text = Constants.CountCoinAntiRocketSystemTypeThree.ToString();
                _textCurrentStateImprovement.text = ProfilePlayer.GetAntiRocketSystem.ToString() + "/4";
                _textDescriptionImprovement.text = "+25% speed your ship";
                if (ProfilePlayer.GetCountCoin >= Constants.CountCoinAntiRocketSystemTypeThree)
                {
                    _buttonBuy.interactable = true;
                }
                else
                {
                    _buttonBuy.interactable = false;
                }
                break;
            case 2:
                ProfilePlayer.EditCountCoin(-Constants.CountCoinAntiRocketSystemTypeThree);
                ProfilePlayer.EditAntiRocketSystem(3);
                _textPriceImprovement.text = Constants.CountCoinAntiRocketSystemTypeFour.ToString();
                _textCurrentStateImprovement.text = ProfilePlayer.GetAntiRocketSystem.ToString() + "/4";
                _textDescriptionImprovement.text = "+40% speed your ship";
                if (ProfilePlayer.GetCountCoin >= Constants.CountCoinAntiRocketSystemTypeFour)
                {
                    _buttonBuy.interactable = true;
                }
                else
                {
                    _buttonBuy.interactable = false;
                }
                break;
            case 3:
                ProfilePlayer.EditCountCoin(-Constants.CountCoinAntiRocketSystemTypeFour);
                ProfilePlayer.EditAntiRocketSystem(4);
                _textPriceImprovement.text = 0.ToString();
                _textCurrentStateImprovement.text = ProfilePlayer.GetAntiRocketSystem.ToString() + "/4";
                _textDescriptionImprovement.text = "Max time";
                _buttonBuy.interactable = false;
                break;
        }
        StateImproveShip.Instance.RefreshState();
    }

}
