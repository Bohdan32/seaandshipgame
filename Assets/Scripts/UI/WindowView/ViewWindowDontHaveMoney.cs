﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewWindowDontHaveMoney : WindowViewBase
{

    [SerializeField]
    private Text _textCountCoin;
	void Start ()
    {
        buttonClose.onClick.AddListener(Close);
	}
    private void OnEnable()
    {
        //_textCountCoin.text = ProfilePlayer.GetCountCoin.ToString();
    }
    public override void Close()
    {
        UnityPoolManager.Instance.Push(LoadWindowManager.eWindowNameInLocation.WindowDontHaveMoney, this);
    }

}
