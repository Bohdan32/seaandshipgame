﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WindowBuyArmor : WindowViewBase
{
    [SerializeField]
    private Button _buttonBuy;

    [SerializeField]
    private Text _textCurrentStateImprovement;
    [SerializeField]
    private Text _textPriceImprovement;
    [SerializeField]
    private Text _textDescriptionImprovement;


    void Start()
    {
        buttonClose.onClick.AddListener(Close);
        _buttonBuy.onClick.AddListener(BuyInmprovement);
    }
     void OnEnable()
    {
        base.OnEnable();
        _textCurrentStateImprovement.text = ProfilePlayer.GetArmor.ToString() + "/4";
       
        switch(ProfilePlayer.GetArmor)
        {
            case 1:
                _textPriceImprovement.text = Constants.CountCoinArmorTypeTwo.ToString();
                _textDescriptionImprovement.text = "+150 health for your ship";
                if (ProfilePlayer.GetCountCoin >= Constants.CountCoinArmorTypeTwo)
                {
                    _buttonBuy.interactable = true;
                }
                else
                {
                    _buttonBuy.interactable = false;
                }
                break;
            case 2:
                _textPriceImprovement.text = Constants.CountCoinArmorTypeThree.ToString();
                _textDescriptionImprovement.text = "+250 health for your ship";
                if (ProfilePlayer.GetCountCoin >= Constants.CountCoinArmorTypeThree)
                {
                    _buttonBuy.interactable = true;
                }
                else
                {
                    _buttonBuy.interactable = false;
                }
                break;
            case 3:
                _textPriceImprovement.text = Constants.CountCoinArmorTypeFour.ToString();
                _textDescriptionImprovement.text = "+350 health for your ship";
                if (ProfilePlayer.GetCountCoin >= Constants.CountCoinArmorTypeFour)
                {
                    _buttonBuy.interactable = true;
                }
                else
                {
                    _buttonBuy.interactable = false;
                }
                break;
            case 4:
                _buttonBuy.interactable = false;
                _textDescriptionImprovement.text = "Max health";
                _textPriceImprovement.text = 0.ToString();
                break;
        }
        
    }
    public override void Close()
    {
        UnityPoolManager.Instance.Push(LoadWindowManager.eWindowNameInLocation.WindowBuyArmor, this);
    }

    void BuyInmprovement()
    {
        
        switch(ProfilePlayer.GetArmor)
        {
            case 1:
                ProfilePlayer.EditCountCoin(-Constants.CountCoinArmorTypeTwo);
                ProfilePlayer.EditArmor(2);
                _textPriceImprovement.text = Constants.CountCoinArmorTypeThree.ToString();
                _textCurrentStateImprovement.text = ProfilePlayer.GetArmor.ToString() + "/4";
                _textDescriptionImprovement.text = "+250 health for your ship";
                if (ProfilePlayer.GetCountCoin >= Constants.CountCoinArmorTypeThree)
                {
                    _buttonBuy.interactable = true;
                }
                else
                {
                    _buttonBuy.interactable = false;
                }
                break;
            case 2:
                ProfilePlayer.EditCountCoin(-Constants.CountCoinArmorTypeThree);
                ProfilePlayer.EditArmor(3);
                _textPriceImprovement.text = Constants.CountCoinArmorTypeFour.ToString();
                _textCurrentStateImprovement.text = ProfilePlayer.GetArmor.ToString() + "/4";
                _textDescriptionImprovement.text = "+350 health for your ship";
                if (ProfilePlayer.GetCountCoin >= Constants.CountCoinArmorTypeFour)
                {
                    _buttonBuy.interactable = true;
                }
                else
                {
                    _buttonBuy.interactable = false;
                }
                break;
            case 3:
                ProfilePlayer.EditCountCoin(-Constants.CountCoinArmorTypeFour);
                ProfilePlayer.EditArmor(4);
                _textPriceImprovement.text = 0.ToString();
                _textCurrentStateImprovement.text = ProfilePlayer.GetArmor.ToString() + "/4";
                _textDescriptionImprovement.text = "Max health";
                _buttonBuy.interactable = false;
                break;
        }
        StateImproveShip.Instance.RefreshState();

    }

}
