﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewWindowBuyNewCaliber : WindowViewBase {

    [SerializeField]
    private Button _buttonBuy;
    [SerializeField]
    private Text _textCurrentStateImprovement;
    [SerializeField]
    private Text _textPriceImprovement;
    [SerializeField]
    private Text _textDescriptionImprovement;

    void Start()
    {
        buttonClose.onClick.AddListener(Close);
        _buttonBuy.onClick.AddListener(BuyInmprovement);
    }
    private void OnEnable()
    {
        base.OnEnable();
        _textCurrentStateImprovement.text = ProfilePlayer.GetCaliber.ToString() + "/4";
        switch (ProfilePlayer.GetCaliber)
        {
            case 1:
                _textPriceImprovement.text = Constants.CountCoinArmorTypeTwo.ToString();
                _textDescriptionImprovement.text = "+5% damage your bullet";
                if (ProfilePlayer.GetCountCoin >= Constants.CountCoinArmorTypeTwo)
                {
                    _buttonBuy.interactable = true;
                }
                else
                {
                    _buttonBuy.interactable = false;
                }
                break;
            case 2:
                _textPriceImprovement.text = Constants.CountCoinCaliberTypeThree.ToString();
                _textDescriptionImprovement.text = "+10% damage your bullet";
                if (ProfilePlayer.GetCountCoin >= Constants.CountCoinCaliberTypeThree)
                {
                    _buttonBuy.interactable = true;
                }
                else
                {
                    _buttonBuy.interactable = false;
                }
                break;
            case 3:
                _textPriceImprovement.text = Constants.CountCoinCaliberTypeFour.ToString();
                _textDescriptionImprovement.text = "+20% damage your bullet";
                if (ProfilePlayer.GetCountCoin >= Constants.CountCoinCaliberTypeFour)
                {
                    _buttonBuy.interactable = true;
                }
                else
                {
                    _buttonBuy.interactable = false;
                }
                break;
            case 4:
                _buttonBuy.interactable = false;
                _textDescriptionImprovement.text = "Max damage";
                _textPriceImprovement.text = 0.ToString();
                break;
        }

    
    }
    public override void Close()
    {
        UnityPoolManager.Instance.Push(LoadWindowManager.eWindowNameInLocation.WindowBuyNewCaliber, this);
    }

    void BuyInmprovement()
    {
        
        switch (ProfilePlayer.GetCaliber)
        {
            case 1:
                ProfilePlayer.EditCountCoin(-Constants.CountCoinCaliberTypeTwo);
                ProfilePlayer.EditCaliber(2);
                _textPriceImprovement.text = Constants.CountCoinCaliberTypeThree.ToString();
                _textCurrentStateImprovement.text = ProfilePlayer.GetCaliber.ToString() + "/4";
                _textDescriptionImprovement.text = "+10% damage your bullet";
                if (ProfilePlayer.GetCountCoin >= Constants.CountCoinCaliberTypeThree)
                {
                    _buttonBuy.interactable = true;
                }
                else
                {
                    _buttonBuy.interactable = false;
                }
                break;
            case 2:
                ProfilePlayer.EditCountCoin(-Constants.CountCoinCaliberTypeThree);
                ProfilePlayer.EditCaliber(3);
                _textPriceImprovement.text = Constants.CountCoinCaliberTypeFour.ToString();
                _textCurrentStateImprovement.text = ProfilePlayer.GetCaliber.ToString() + "/4";
                _textDescriptionImprovement.text = "+20% damage your bullet";
                if (ProfilePlayer.GetCountCoin >= Constants.CountCoinCaliberTypeFour)
                {
                    _buttonBuy.interactable = true;
                }
                else
                {
                    _buttonBuy.interactable = false;
                }
                break;
            case 3:
                ProfilePlayer.EditCountCoin(-Constants.CountCoinCaliberTypeFour);
                ProfilePlayer.EditCaliber(4);
                _textPriceImprovement.text = 0.ToString();
                _textCurrentStateImprovement.text = ProfilePlayer.GetCaliber.ToString() + "/4";
                _textDescriptionImprovement.text = "Max damage";
                _buttonBuy.interactable = false;
                break;
        }
        StateImproveShip.Instance.RefreshState();
    }

}
