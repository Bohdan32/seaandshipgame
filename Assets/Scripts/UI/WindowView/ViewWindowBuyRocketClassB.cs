﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewWindowBuyRocketClassB : WindowViewBase
{

    [SerializeField]
    private Button _buttonBuy;
    [SerializeField]
    private Text _textCountCoin;
    [SerializeField]
    private Button _buttonAddRocket;
    [SerializeField]
    private Button _buttonReduceRocket;
    [SerializeField]
    private Text _textCountRocket;
    int countRocket;
    int countCoin;

    void Start()
    {
        buttonClose.onClick.AddListener(Close);
        _buttonBuy.onClick.AddListener(BuyInmprovement);
        _buttonAddRocket.onClick.AddListener(AddRocket);
        _buttonReduceRocket.onClick.AddListener(ReduceRocket);

    }
    private void OnEnable()
    {
        base.OnEnable();
        countRocket = 1;
        countCoin = countRocket * Constants.CountCoinRocketTypeTwo;
        _textCountRocket.text = countRocket.ToString();
        _textCountCoin.text = CalculatorCounCoin(countRocket).ToString();
        if (ProfilePlayer.GetCountCoin >= countCoin)
        {
            _buttonBuy.interactable = true;
        }
        else
        {
            _buttonBuy.interactable = false;
        }
    }
    void AddRocket()
    {
        countRocket++;
        _textCountRocket.text = countRocket.ToString();
        _textCountCoin.text = CalculatorCounCoin(countRocket).ToString();
        countCoin = countRocket * Constants.CountCoinRocketTypeTwo;
        if (ProfilePlayer.GetCountCoin >= countCoin)
        {
            _buttonBuy.interactable = true;
        }
        else
        {
            _buttonBuy.interactable = false;
        }
    }
    void ReduceRocket()
    {
        if (countRocket > 0)
        {
            countRocket--;
            _textCountRocket.text = countRocket.ToString();
            _textCountCoin.text = CalculatorCounCoin(countRocket).ToString();
            countCoin = countRocket * Constants.CountCoinRocketTypeTwo;
            if (ProfilePlayer.GetCountCoin >= countCoin)
            {
                _buttonBuy.interactable = true;
            }
            else
            {
                _buttonBuy.interactable = false;
            }
        }
        else
        {
            countRocket = 0;
            _textCountRocket.text = countRocket.ToString();
            _textCountCoin.text = CalculatorCounCoin(countRocket).ToString();
            countCoin = countRocket * Constants.CountCoinRocketTypeTwo;
        }
    }
    int CalculatorCounCoin(int count)
    {
        int b = count * Constants.CountCoinRocketTypeTwo;
        return b;
    }
    public override void Close()
    {
        UnityPoolManager.Instance.Push(LoadWindowManager.eWindowNameInLocation.WindowBuyRocketClassB, this);
    }

    void BuyInmprovement()
    {

            ProfilePlayer.EditCountCoin(-countCoin);
            ProfilePlayer.EditRocketClassB(countRocket);
            StateImproveShip.Instance.RefreshState();
            countRocket = 1;
            countCoin = countRocket * Constants.CountCoinRocketTypeTwo;
            _textCountRocket.text = countRocket.ToString();
            _textCountCoin.text = CalculatorCounCoin(countRocket).ToString();
        

    }

}
