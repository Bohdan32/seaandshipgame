﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewWindowBuyEngine : WindowViewBase 
{

    [SerializeField]
    private Button _buttonBuy;
    [SerializeField]
    private Text _textCurrentStateImprovement;
    [SerializeField]
    private Text _textPriceImprovement;
    [SerializeField]
    private Text _textDescriptionImprovement;

    void Start()
    {
      
        buttonClose.onClick.AddListener(Close);
        _buttonBuy.onClick.AddListener(BuyInmprovement);
    }
    private void OnEnable()
    {
        base.OnEnable();
        
        _textCurrentStateImprovement.text = ProfilePlayer.GetEngineImproved.ToString() + "/4";

        switch (ProfilePlayer.GetEngineImproved)
        {
            case 1:
                _textPriceImprovement.text = Constants.CountCoinEngineTypeTwo.ToString();
                _textDescriptionImprovement.text = "+15% speed your ship";
                if (ProfilePlayer.GetCountCoin >= Constants.CountCoinEngineTypeTwo)
                {
                    _buttonBuy.interactable = true;
                }
                else
                {
                    _buttonBuy.interactable = false;
                }
                break;
            case 2:
                _textPriceImprovement.text = Constants.CountCoinEngineTypeThree.ToString();
                _textDescriptionImprovement.text = "+25% speed your ship";
                if (ProfilePlayer.GetCountCoin >= Constants.CountCoinEngineTypeThree)
                {
                    _buttonBuy.interactable = true;
                }
                else
                {
                    _buttonBuy.interactable = false;
                }
                break;
            case 3:
                _textPriceImprovement.text = Constants.CountCoinEngineTypeFour.ToString();
                _textDescriptionImprovement.text = "40% speed your ship";
                if (ProfilePlayer.GetCountCoin >= Constants.CountCoinEngineTypeFour)
                {
                    _buttonBuy.interactable = true;
                }
                else
                {
                    _buttonBuy.interactable = false;
                }
                break;
            case 4:
                _buttonBuy.interactable = false;
                _textDescriptionImprovement.text = "Max speed";
                _textPriceImprovement.text = 0.ToString();
                break;
        }
    }
    public override void Close()
    {
        UnityPoolManager.Instance.Push(LoadWindowManager.eWindowNameInLocation.WindowBuyEngine, this);
    }

    void BuyInmprovement()
    {
        
        switch (ProfilePlayer.GetEngineImproved)
        {
            case 1:
                ProfilePlayer.EditCountCoin(-Constants.CountCoinEngineTypeTwo);
                ProfilePlayer.EditEngineImproved(2);
                _textPriceImprovement.text = Constants.CountCoinEngineTypeThree.ToString();
                _textCurrentStateImprovement.text = ProfilePlayer.GetEngineImproved.ToString() + "/4";
                _textDescriptionImprovement.text = "+25% speed your ship";
                if (ProfilePlayer.GetCountCoin >= Constants.CountCoinEngineTypeThree)
                {
                    _buttonBuy.interactable = true;
                }
                else
                {
                    _buttonBuy.interactable = false;
                }
                break;
            case 2:
                ProfilePlayer.EditCountCoin(-Constants.CountCoinEngineTypeThree);
                ProfilePlayer.EditEngineImproved(3);
                _textPriceImprovement.text = Constants.CountCoinEngineTypeFour.ToString();
                _textCurrentStateImprovement.text = ProfilePlayer.GetEngineImproved.ToString() + "/4";
                _textDescriptionImprovement.text = "+40% speed your ship";
                if (ProfilePlayer.GetCountCoin >= Constants.CountCoinEngineTypeFour)
                {
                    _buttonBuy.interactable = true;
                }
                else
                {
                    _buttonBuy.interactable = false;
                }
                break;
            case 3:
                ProfilePlayer.EditCountCoin(-Constants.CountCoinEngineTypeFour);
                ProfilePlayer.EditEngineImproved(4);
                _textPriceImprovement.text = 0.ToString();
                _textCurrentStateImprovement.text = ProfilePlayer.GetEngineImproved.ToString() + "/4";
                _textDescriptionImprovement.text = "Max speed";
                _buttonBuy.interactable = false;
                break;
        }
        StateImproveShip.Instance.RefreshState();
    }

}
