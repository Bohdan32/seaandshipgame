﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewUIPanelWindow : WindowViewBase
{
    [SerializeField]
    private ViewHealthPanel _healthPanel;
    [SerializeField]
    private ViewDistancePanel _distancePanel;
    [SerializeField]
    private ViewRocketPanel _rocketPanel;
    [SerializeField]
    private ViewProtectionPanel _protectionPanel;

    void Start()
    {
        
    }

    public void Construct()
    {
        _healthPanel.Construct(15);
        _distancePanel.Construct(1);
        _rocketPanel.Construct(1, 2);
        _protectionPanel.Construct(10f);
    }

    public override void Close()
    {
        UnityPoolManager.Instance.Push(LoadWindowManager.eWindowNameInLocation.UIPanel, this);
    }
}
