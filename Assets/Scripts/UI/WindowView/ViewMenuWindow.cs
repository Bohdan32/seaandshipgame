﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ViewMenuWindow : WindowViewBase
{
    public override void OnEnable()
    {
        content.GetComponent<CanvasGroup>().DOFade(1, 0.5f);

        gameObject.transform.SetSiblingIndex(gameObject.transform.parent.transform.childCount);
    }

    public override void Close()
    {
        UnityPoolManager.Instance.Push(LoadWindowManager.eWindowNameInLocation.Menu, this);
    }
}
