﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewGameOverWindow : WindowViewBase
{
    [SerializeField]
    private Button _buttonRestart;
    [SerializeField]
    private Text _txtScore;

    void Start()
    {
        base.Start();
        buttonClose.onClick.AddListener(Close);
        _buttonRestart.onClick.AddListener(RestartGame);
    }

    public void Construct(float score)
    {
        //ProfilePlayer.CurrentScore = score;
        //_txtScore.text = "You reached" + score.ToString();
    }
    public override void Close()
    {
        UnityPoolManager.Instance.Push(LoadWindowManager.eWindowNameInLocation.GameOver, this);
        UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.Menu);
    }
    public void RestartGame()
    {
        UnityPoolManager.Instance.Push(LoadWindowManager.eWindowNameInLocation.GameOver, this);
        //UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.Field);
    }

}
