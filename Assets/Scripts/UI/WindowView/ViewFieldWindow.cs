﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.UI;
using System;
using System.Threading;



public class ViewFieldWindow : MonoBehaviour
{
    
    public static ViewFieldWindow Instance;
    [SerializeField]
    private Camera _cam;
    [SerializeField]
    private GameObject _empty;

    [SerializeField]
    private Ship _ship;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        _cam = GameObject.Find("Camera").GetComponent<Camera>();
        Screen.autorotateToLandscapeLeft = false;
        Screen.autorotateToLandscapeRight = false;
        Screen.autorotateToPortrait = false;
        Screen.autorotateToPortraitUpsideDown = false;
    }

    public void Construct()
    {
        _ship.Construct(15);
    }

    public void GameOver()
    {
        ViewDistancePanel.Instance.PauseTimer();
        UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.UIPanel).GetComponent<ViewUIPanelWindow>().Close();
        UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.GameOver);
        UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.GameOver).
               GetComponent<ViewGameOverWindow>().Construct(ViewDistancePanel.Instance.CurrentDistance);
        gameObject.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Vector3 MousePos;
            MousePos = _cam.ScreenToWorldPoint(Input.mousePosition);
            _empty.transform.parent = _empty.transform.parent.parent.parent;
            _empty.transform.localPosition = MousePos;
            _empty.transform.parent = _ship.transform;
            MousePos = _empty.transform.localPosition;
            _ship.Shoot(MousePos);
        }
    }

}
